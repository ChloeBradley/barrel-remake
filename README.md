# A remake of my website

Relying on tools is cool until you understand the limitations...

## Mediawiki is terrible
 * the spam bots send help
 * project just doesn't have enough contributors to justify dealing with mediawiki limitations
 * semantic mediawiki is a neat tool that too few editors can actually learn to use, requires pages for every little thing
 * plugins needed for everything, even "basic" features
 * wikiseo was not fun to fix, and it was just a & -> &amp; -> &amp;amp; conversion with two changes necessary to fix
 * markup gets weird when you need behaviors that are outside of the norm
 * creating multiple wikis with one userbase is so obnoxious and unintuitive that I had to write down the process so I wouldn't WHOOPSIE at some point... said instructions are in another repository
 
## Wordpress is annoying
 * update -> break everything whoops
 * update -> settings reverted to default whoops
 * plugins for everything, can't even have responsive tables without a plugin
 * getting pics to display nicely can be a problem
 * have to edit my .htaccess whenever I add a new category because the default wordpress behavior is to pretend my mediawiki pages don't exist
 
## Doing it myself!
 * I was told to learn angular. forcefully.
 * looks neat
 * I refuse to accept anything less than beautiful on both mobile and desktop